require 'test_helper'

class BeverageControllerTest < ActionDispatch::IntegrationTest
  test "should get add" do
    get beverage_add_url
    assert_response :success
  end

  test "should get list" do
    get beverage_list_url
    assert_response :success
  end

  test "should get edit" do
    get beverage_edit_url
    assert_response :success
  end

  test "should get destroy" do
    get beverage_destroy_url
    assert_response :success
  end

end
