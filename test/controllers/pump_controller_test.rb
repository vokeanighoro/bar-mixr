require 'test_helper'

class PumpControllerTest < ActionDispatch::IntegrationTest
  test "should get add" do
    get pump_add_url
    assert_response :success
  end

  test "should get list" do
    get pump_list_url
    assert_response :success
  end

  test "should get edit" do
    get pump_edit_url
    assert_response :success
  end

  test "should get destroy" do
    get pump_destroy_url
    assert_response :success
  end

end
