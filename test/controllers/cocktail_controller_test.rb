require 'test_helper'

class CocktailControllerTest < ActionDispatch::IntegrationTest
  test "should get list" do
    get cocktail_list_url
    assert_response :success
  end

  test "should get add_to_queue" do
    get cocktail_add_to_queue_url
    assert_response :success
  end

  test "should get remove_from_queue" do
    get cocktail_remove_from_queue_url
    assert_response :success
  end

  test "should get queue_details" do
    get cocktail_queue_details_url
    assert_response :success
  end

end
