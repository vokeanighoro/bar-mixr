Rails.application.routes.draw do
  root 'bar#index'

  get '/login' => 'session#index', as: 'login'

  get '/sign_up' => 'session#new'

  post '/sign_up' => 'session#create'

  post 'session/authorise'

  get 'session/destroy'

  post 'session/update'

  get 'session/forgot_password'

  resources :beverages

  resources :pumps

  resources :cocktails

  resources :bar

  post '/cocktails/order/:id' => 'cocktails#order', as: 'order_cocktail'

  get '/admin' => 'admin#index', as: 'admin'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
