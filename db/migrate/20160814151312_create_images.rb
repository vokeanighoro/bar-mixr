class CreateImages < ActiveRecord::Migration[5.0]
  def change
    create_table :images do |t|
      t.references :cocktail, foreign_key: true
      t.text :file_path

      t.timestamps
    end
  end
end
