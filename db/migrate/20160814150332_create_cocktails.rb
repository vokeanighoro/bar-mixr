class CreateCocktails < ActiveRecord::Migration[5.0]
  def change
    create_table :cocktails do |t|
      t.string :name
      t.text :description
      t.decimal :price, :precision => 5, :scale => 2

      t.timestamps
    end
  end
end
