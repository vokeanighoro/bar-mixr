class CreateStocks < ActiveRecord::Migration[5.0]
  def change
    create_table :stocks do |t|
      t.references :beverage, foreign_key: true
      t.decimal :level, :precision => 5, :scale => 2

      t.timestamps
    end
  end
end
