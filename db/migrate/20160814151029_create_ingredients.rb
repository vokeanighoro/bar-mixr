class CreateIngredients < ActiveRecord::Migration[5.0]
  def change
    create_table :ingredients do |t|
      t.references :cocktail, foreign_key: true
      t.references :beverage, foreign_key: true
      t.integer :measurement

      t.timestamps
    end
  end
end
