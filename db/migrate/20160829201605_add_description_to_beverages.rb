class AddDescriptionToBeverages < ActiveRecord::Migration[5.0]
  def change
    add_column :beverages, :description, :text
  end
end
