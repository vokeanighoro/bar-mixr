class User < ApplicationRecord
  has_secure_password
  has_many :orders
  validates :name, presence: true
  validates :email, presence: true
  validates_format_of :email, :with => /.+@.+\..+/i
  PASSWORD_FORMAT = /\A
  (?=.{8,})          # Must contain 8 or more characters
  (?=.*\d)           # Must contain a digit
  (?=.*[a-z])        # Must contain a lower case character
  (?=.*[A-Z])        # Must contain an upper case character
/x
  validates :password, length: { within: 8..40 }, format: { with: PASSWORD_FORMAT, message: "Must contain a digit, lower and upper case character" }, on: :create
end
