class Cocktail < ApplicationRecord
  has_many :ingredients, :dependent => :destroy, inverse_of: :cocktail
  has_many :images, :dependent => :destroy, inverse_of: :cocktail
  validates :name, :price, :presence => true
  accepts_nested_attributes_for :ingredients, :allow_destroy => true, :reject_if => proc { |d| d['beverage_id'].blank? }
  accepts_nested_attributes_for :images, :allow_destroy => true, :reject_if => proc { |d|
    d.empty? || d['uploaded_file'].class != ActionDispatch::Http::UploadedFile
  }
end
