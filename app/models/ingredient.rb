class Ingredient < ApplicationRecord
  belongs_to :cocktail, inverse_of: :ingredients
  belongs_to :beverage
end
