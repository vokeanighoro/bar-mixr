class Beverage < ApplicationRecord
  belongs_to :pump
  validates :name, presence: true, allow_blank: false
  validates :pump_id, presence: true, allow_blank: false
end
