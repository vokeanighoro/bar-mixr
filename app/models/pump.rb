class Pump < ApplicationRecord
  has_many :beverages
  validates :port, numericality: { only_integer: true }
  validates :port, presence: true, allow_blank: false
end
