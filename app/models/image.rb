class Image < ApplicationRecord
  belongs_to :cocktail, inverse_of: :images

  def uploaded_file=(incoming_file)
    self.content_type = incoming_file.content_type
    FileUtils.mkdir_p(upload_dir)
    filename = "upload_#{SecureRandom.uuid}"
    write_path = File.join(upload_dir, filename)
    read_path = File.join(file_read_path, filename)
    File.open(write_path, "wb") { |f| f.write(incoming_file.read) }
    self.file_path = read_path
  end

  def filename=(new_filename)
    write_attribute("filename", sanitize_filename(new_filename))
  end

  private

  def upload_dir
    'public/images/upload'
  end

  def file_read_path
    '/images/upload'
  end

  def content_format
    if !["image/jpg", "image/jpeg", "image/gif", "image/tif", "image/png"].include?(self.content_type.downcase)
      errors.add("Attachment Format", "should be one of the following: GIF, TIF, JPG, JPEG or PNG")
      return false
    else
      return true
    end
  end
end
