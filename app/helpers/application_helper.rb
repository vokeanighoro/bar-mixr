module ApplicationHelper
  def set_active_link(url: nil, ctrler: nil)
    ' class=fh5co-active' if url && current_page?(url) || ctrler && controller.controller_name == ctrler.to_s
  end

  def active_tab(ctrler:, action:)
    ' class=active' if controller.controller_name == ctrler.to_s && params[:action] == action
  end

  def render_sub_tabs
    case controller.controller_name
    when 'pumps'
      render 'pumps/tabs'
    when 'beverages'
      render 'beverages/tabs'
    when 'cocktails'
      render 'cocktails/tabs'
    end
  end
end
