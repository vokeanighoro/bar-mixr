class SessionController < ApplicationController
  skip_before_action :require_login

  def index
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    if @user.save
      session[:user_id] = @user.id
      flash[:success] = '<span class="ti-check"></span> Account Successfully Registered. Enjoy the bar!'
      redirect_to root_url
    else
      render :new
    end
  end

  def authorise
    user = User.find_by_email(params[:user][:email])
    # If the user exists AND the password entered is correct.
    if user && user.authenticate(params[:user][:password])
      # Save the user id inside the browser cookie. This is how we keep the user
      # logged in when they navigate around our website.
      session[:user_id] = user.id
      redirect_to '/'
    else
      flash[:danger] = '<span class="ti-alert"></span> We do not recognise those details. Please try again.'
      redirect_to login_url
    end
  end

  def destroy
    session[:user_id] = nil
    redirect_to login_url
  end

  def update
  end

  def forgot_password
  end

  private

  def user_params
    params.require(:user).permit(:name, :email, :password, :password_confirmation)
  end
end
