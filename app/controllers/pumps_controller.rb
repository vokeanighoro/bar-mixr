class PumpsController < ApplicationController
  def index
    @pumps = Pump.all
  end

  def new
    @pump = Pump.new
  end

  def create
    @pump = Pump.new(pump_params)
    if @pump.save
      flash[:success] = "<span class='ti-check'></span> GPIO Port '#{@pump.port}' successfully registered"
      redirect_to pumps_url
    else
      render action: 'new'
    end
  end

  def update
    @pump = Pump.find(params[:id])
    @pump.port = pump_params[:port]
    if @pump.save
      flash[:success] = "<span class='ti-check'></span> GPIO Port '#{@pump.port}' successfully updated"
      redirect_to pumps_url
    else
      render action: 'edit'
    end
  end

  def edit
    @pump = Pump.find(params[:id])
  end

  private

  def pump_params
    params.require(:pump).permit(:port)
  end
end
