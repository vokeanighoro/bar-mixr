class BeveragesController < ApplicationController
  def index
    @beverages = Beverage.all
  end

  def new
    @beverage = Beverage.new
    @available_pumps = available_pumps
    @available_pumps.compact!
  end

  def create
    @beverage = Beverage.new(beverage_params)
    if @beverage.save
      flash[:success] = "<span class='ti-check'></span> Beverage '#{@beverage.name}' successfully added and registered on port #{@beverage.pump.port}"
      redirect_to beverages_url
    else
      @available_pumps = available_pumps
      @available_pumps.compact!
      render action: 'new'
    end
  end

  def update
    @beverage = Beverage.find(params[:id])
    @beverage.name = beverage_params[:name]
    @beverage.description = beverage_params[:description]
    @beverage.pump_id = beverage_params[:pump_id]
    if @beverage.save
      flash[:success] = "<span class='ti-check'></span> Beverage '#{@beverage.name}' successfully updated and registered on port #{@beverage.pump.port}"
      redirect_to beverages_url
    else
      @available_pumps = available_pumps
      @available_pumps.push([@beverage.pump.port, @beverage.pump.id])
      @available_pumps.compact!
      render action: 'edit'
    end
  end

  def edit
    @beverage = Beverage.find(params[:id])
    @available_pumps = available_pumps
    @available_pumps.push([@beverage.pump.port, @beverage.pump.id])
    @available_pumps.compact!
  end

  def destroy
    @beverage = Beverage.find(params[:id])
    if @beverage.destroy
      flash[:success] = "<span class='ti-check'></span> Beverage '#{@beverage.name}' successfully removed"
    end
    redirect_to beverages_url
  end

  private

  def beverage_params
    params.require(:beverage).permit(:name, :description, :pump_id)
  end
end
