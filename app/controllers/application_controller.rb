class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :require_login

  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end

  def available_pumps
    Pump.all.map do |s|
      [s.port, s.id] unless s.beverages.size > 0
    end
  end

  def available_beverages
    Beverage.all.map do |b|
      [b.name, b.id]
    end
  end
  helper_method :current_user, :available_beverages, :available_pumps

  private

  def require_login
    unless current_user
      flash[:warning] = "You have to be logged in to access the bar."
      redirect_to login_url
    end
  end
end
