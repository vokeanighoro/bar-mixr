class CocktailsController < ApplicationController
  def index
    @cocktails = Cocktail.all
  end

  def list
  end

  def new
    @cocktail = Cocktail.new
    5.times do
      @cocktail.images.build
      @cocktail.ingredients.build
    end
    @beverages = available_beverages
  end

  def create
    @cocktail = Cocktail.new(cocktail_params)
    if @cocktail.save
      flash[:success] = "<span class='ti-check'></span> Cocktail '#{@cocktail.name}' successfully added"
      redirect_to cocktails_url
    else
      @beverages = available_beverages
      render action: 'new'
    end
  end

  def destroy
    cocktail = Cocktail.find(params[:id])
    if cocktail.destroy
      flash[:success] = "<span class='ti-check'></span> Cocktail '#{cocktail.name}' successfully deleted"
    end
    redirect_to cocktails_url
  end

  def order

  end

  private

  def cocktail_params
    params.require(:cocktail).permit(:name, :description, :price, images_attributes:[:uploaded_file], ingredients_attributes:[:beverage_id, :measurement])
  end
end
